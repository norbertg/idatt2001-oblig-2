package edu.ntnu.idatt2001.oblig2;

import edu.ntnu.idatt2001.oblig2.memberships.BasicMembership;
import edu.ntnu.idatt2001.oblig2.memberships.GoldMembership;
import edu.ntnu.idatt2001.oblig2.memberships.SilverMembership;

import java.time.LocalDate;

/**
 * BonusMember represents a customer registered for the bonus points programme.
 */
public class BonusMember {
    // Membership limits. These represent the required amount of points to transfer to the
    // specified level.
    // E.g. When a member passes a "Rainbow Limit" of 13_370 points, the member will be
    // upgraded to the "Rainbow Membership".
    private final static int SILVER_LIMIT = 25_000;
    private final static int GOLD_LIMIT = 75_000;

    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance;
    private String name;
    private String emailAddress;
    private String password;
    private Membership membership;

    /**
     * Creates a new BonusMember.
     * @param memberNumber The member number.
     * @param enrolledDate The date of enrollment.
     * @param bonusPoints The current amount of bonus points.
     * @param name The members name.
     * @param emailAddress The email address of the member.
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPoints, String name, String emailAddress) {
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPoints;
        this.name = name;
        this.emailAddress = emailAddress;
        checkAndSetMembership();
    }

    /**
     * Creates a new BonusMember with a password.
     * @param memberNumber The member number.
     * @param enrolledDate The date of enrollment.
     * @param bonusPoints The current amount of bonus points.
     * @param name The members name.
     * @param emailAddress The email address of the member.
     * @param password The password of the member.
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPoints, String name, String emailAddress, String password) {
        this(memberNumber, enrolledDate, bonusPoints, name, emailAddress);
        this.password = password;
    }

    /**
     * Gets the member number.
     * @return The member number.
     */
    public int getMemberNumber() {
        return memberNumber;
    }

    /**
     * Gets the date of enrollment.
     * @return the date of enrollment.
     */
    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    /**
     * Gets the bonus points balance.
     * @return The bonus points balance.
     */
    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }

    /**
     * Gets the member's name.
     * @return The name of the member.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the email address of the member.
     * @return The email address of the member.
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Gets the active membership.
     * @return The active membership.
     */
    public Membership getMembership() {
        return membership;
    }

    /**
     * Checks that the provided password is correct.
     * @param password The password to check for correctness.
     * @return Whether the password is correct (true) or not (false).
     */
    public boolean checkPassword(String password) {
        return this.password.equals(password);
    }

    /**
     * Registers new bonus points.
     * @param newPoints The amount of bonus points to register.
     */
    public void registerBonusPoints(int newPoints) {
        if (newPoints < 0) {
            throw new IllegalArgumentException("newPoints cannot be negative");
        }
        bonusPointsBalance = membership.registerPoints(bonusPointsBalance, newPoints);
        checkAndSetMembership();
    }

    /**
     * Gets the name of the current membership level.
     * @return The membership name.
     */
    public String getMembershipLevel() {
        return membership.getMembershipName();
    }

    private void checkAndSetMembership() {
        if (bonusPointsBalance >= GOLD_LIMIT) {
            membership = new GoldMembership();
        } else if (bonusPointsBalance >= SILVER_LIMIT) {
            membership = new SilverMembership();
        } else {
            membership = new BasicMembership();
        }
    }

    @Override
    public String toString() {
        return "BonusMember{" +
                "memberNumber=" + memberNumber +
                ", enrolledDate=" + enrolledDate +
                ", bonusPointsBalance=" + bonusPointsBalance +
                ", name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                ", membership=" + membership +
                '}';
    }
}
