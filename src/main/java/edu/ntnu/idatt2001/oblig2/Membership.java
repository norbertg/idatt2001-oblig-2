package edu.ntnu.idatt2001.oblig2;

/**
 * Membership level
 */
public abstract class Membership {
    /**
     * Calculates the new bonus point balance.
     * @param bonusPointBalance The current bonus point balance.
     * @param newPoints The amount new points the customer has acquired.
     * @return The new balance.
     */
    public abstract int registerPoints(int bonusPointBalance, int newPoints);

    /**
     * Gets the name of the membership.
     * @return Name of the membership.
     */
    public abstract String getMembershipName();

    @Override
    public String toString() {
        return getMembershipName();
    }
}
