package edu.ntnu.idatt2001.oblig2.memberships;

import edu.ntnu.idatt2001.oblig2.Membership;

/**
 * Basic membership level.
 */
public class BasicMembership extends Membership {
    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        return bonusPointBalance + newPoints;
    }

    @Override
    public String getMembershipName() {
        return "Basic";
    }
}
