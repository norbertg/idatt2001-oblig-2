package edu.ntnu.idatt2001.oblig2.memberships;

import edu.ntnu.idatt2001.oblig2.Membership;

/**
 * Gold membership level.
 */
public class GoldMembership extends Membership {
    private static final float POINTS_SCALING_FACTOR_LEVEL_1 = 1.3f;
    private static final float POINTS_SCALING_FACTOR_LEVEL_2 = 1.5f;
    private static final float POINTS_SCALING_FACTOR_LEVEL_2_ABOVE = 90_000;

    private float getScaleFactorForBalance(int balance) {
        if (balance >= POINTS_SCALING_FACTOR_LEVEL_2_ABOVE) {
            return POINTS_SCALING_FACTOR_LEVEL_2;
        } else {
            return POINTS_SCALING_FACTOR_LEVEL_1;
        }
    }

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        float scaleFactor = getScaleFactorForBalance(bonusPointBalance);
        return bonusPointBalance + Math.round(newPoints * scaleFactor);
    }

    @Override
    public String getMembershipName() {
        return "Gold";
    }
}
