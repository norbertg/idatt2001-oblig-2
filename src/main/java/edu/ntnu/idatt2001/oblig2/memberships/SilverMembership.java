package edu.ntnu.idatt2001.oblig2.memberships;

import edu.ntnu.idatt2001.oblig2.Membership;

/**
 * Silver membership level.
 */
public class SilverMembership extends Membership {
    private static final float POINT_SCALING_FACTOR = 1.2f;

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        return bonusPointBalance + Math.round(newPoints * POINT_SCALING_FACTOR);
    }

    @Override
    public String getMembershipName() {
        return "Silver";
    }
}
