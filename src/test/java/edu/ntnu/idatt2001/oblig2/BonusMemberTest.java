package edu.ntnu.idatt2001.oblig2;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BonusMemberTest {
    @Test
    void constructor_membership_0PointsIsBasicMembership() {
        BonusMember member = new BonusMember(
                1,
                LocalDate.now(),
                0,
                "John McDonald",
                "jmc@gmail.com");

        String membershipLevel = member.getMembershipLevel();

        assertEquals("Basic", membershipLevel);
    }

    @Test
    void constructor_membership_25000PointsIsSilverMembership() {
        BonusMember member = new BonusMember(
                1,
                LocalDate.now(),
                25_000,
                "John McDonald",
                "jmc@gmail.com");

        String membershipLevel = member.getMembershipLevel();

        assertEquals("Silver", membershipLevel);
    }

    @Test
    void constructor_membership_75000PointsIsGoldMembership() {
        BonusMember member = new BonusMember(
                1,
                LocalDate.now(),
                75_000,
                "John McDonald",
                "jmc@gmail.com");

        String membershipLevel = member.getMembershipLevel();

        assertEquals("Gold", membershipLevel);
    }


    @Test
    void registerBonusPoints_negativePoints_throwsException() {
        BonusMember member = new BonusMember(
                1,
                LocalDate.now(),
                0,
                "John McDonald",
                "jmc@gmail.com");

        assertThrows(IllegalArgumentException.class, () -> member.registerBonusPoints(-1));
    }

    @Test
    void registerBonusPoints_zeroPoints_doesNotChangePoints() {
        BonusMember member = new BonusMember(
                1,
                LocalDate.now(),
                0,
                "John McDonald",
                "jmc@gmail.com");
        int wantPoints = member.getBonusPointsBalance();

        member.registerBonusPoints(0);
        int gotPoints = member.getBonusPointsBalance();

        assertEquals(wantPoints, gotPoints);
    }

    @Test
    void registerBonusPoints_basicToSilver_changesMembershipToSilverWhenPointsReach25000() {
        BonusMember member = new BonusMember(
                1,
                LocalDate.now(),
                24_999,
                "John McDonald",
                "jmc@gmail.com");

        member.registerBonusPoints(1);

        assertEquals("Silver", member.getMembershipLevel());
    }

    @Test
    void registerBonusPoints_basicToSilver_changesMembershipToSilverWhenPointsReach75000() {
        BonusMember member = new BonusMember(
                1,
                LocalDate.now(),
                74_999,
                "John McDonald",
                "jmc@gmail.com");

        member.registerBonusPoints(1);

        assertEquals("Gold", member.getMembershipLevel());
    }

}